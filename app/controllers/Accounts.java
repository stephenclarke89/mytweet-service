package controllers;

import java.util.UUID;

import play.*;
import play.mvc.*;
import models.*;

public class Accounts extends Controller
{
  public static void index()
  {
    render();
  }

  public static void signup()
  {
    render();
  }

  public static void register(String firstName, String lastName, String email, String password)
  {
    Logger.info( firstName + " " + lastName + " " + email + " " + password);
    String id = UUID.randomUUID().toString();
    Tweeter tweeter = new Tweeter(id, firstName, lastName, email, password);
    tweeter.save();
    index();
  }

  public static void login()
  {
    render();
  }

  public static void logout()
  {
    session.clear();
    index();
  }

  public static void authenticate(String email, String password)
  {
    Logger.info("Attempting to authenticate with " + email + ":" + password);

    Tweeter user = Tweeter.findByEmail(email);
    if ((user != null) && (user.checkPassword(password) == true))
    {
      Logger.info("Successfull authentication of  " + user.firstName + " " + user.lastName);
      session.put("logged_in_userid", user.id);
      Tweets.index();
    }
    else
    {
      Logger.info("Authentication failed");
      login();
    }
  }

  public static Tweeter getCurrentUser()
  {
	  String userId = session.get("logged_in_userid");
	    if (userId == null)
	    {
	      index();
	    }
	    Tweeter logged_in_user = Tweeter.findById(userId);
	    Logger.info("In Accounts controller: Logged in user is " + logged_in_user.firstName);
	    return logged_in_user;
  }
}