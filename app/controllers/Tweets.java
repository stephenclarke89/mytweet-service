package controllers;
import play.*;
import play.mvc.*;

import java.text.DateFormat;
import java.util.*;

import models.*;

public class Tweets extends Controller
{
  public static void index()
  {
    Tweeter tweeter = Accounts.getCurrentUser();
    List<Tweet> tweets = tweeter.tweets;
    render(tweeter, tweets);
  }

  public static void createtweet(String messageTweet, String contact)
  {
    Tweeter tweeter = Accounts.getCurrentUser();
    String date = setDate();
    String id = UUID.randomUUID().toString();
    Tweet tweet = new Tweet(id, date, messageTweet, contact, tweeter);
    tweeter.tweets.add(tweet);
    tweeter.save();
    index();
  }

  public static void deleteTweet(String id) 
  {
		Tweet tweet = Tweet.findById(id);
		if (tweet == null) 
		{
		  notFound();
		} 
		else 
		{
		  tweet.delete();
		  index();
		}
  }
  
  private static String setDate()
  {
    DateFormat date = DateFormat.getDateTimeInstance();
    date.setTimeZone(TimeZone.getTimeZone("GMT"));
    String formattedDate = date.format(new Date());
    return formattedDate;
  }
  
  public static void renderlist()
  {
	Tweeter tweeter = Accounts.getCurrentUser();
	List<Tweet> tweets  = tweeter.tweets;
    render(tweets);
  }
}