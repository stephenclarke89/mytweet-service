package controllers;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import models.Tweet;
import models.Tweeter;
import play.mvc.Controller;
import utils.GsonStrategy;

public class TweetersAPI extends Controller
{
  static Gson gson = new GsonBuilder()
  .setExclusionStrategies(new GsonStrategy())
  .create();

  public static void getAllTweeters()
  {
    List<Tweeter> Tweeters = Tweeter.findAll();
    renderJSON(gson.toJson(Tweeters));
  }

  public static void getTweeter(String id)
  {
    Tweeter tweeter = Tweeter.findById(id);
    if (tweeter == null)
    {
      notFound();
    }
    else
    {
      renderJSON(gson.toJson(tweeter));
    }
  }

  public static void createTweeter(JsonElement body)
  {
    Tweeter tweeter = gson.fromJson(body.toString(), Tweeter.class);
    tweeter.save();
    renderJSON(gson.toJson(tweeter));
  }

  public static void deleteTweeter(String id)
  {
    Tweeter tweeter = Tweeter.findById(id);
    if (tweeter == null)
    {
      notFound("No Tweeter with ID" + id);
    }
    else
    {
      tweeter.delete();
      renderJSON(gson.toJson(tweeter));
    }
  }

  /**
   * This method deletes all tweeters and tweets.
   */
  
  public static void deleteAllTweeters()
  {
    List<Tweeter> tweeters = Tweeter.findAll();
    int numberTweeters = tweeters.size();
    for(int i = 0; i < numberTweeters; i += 1)
    {
      Tweeter tweeter = tweeters.get(i);
      List<Tweet> tweets = tweeter.tweets;
      for(int j = 0; j < tweets.size(); j += 1)
      {
        Tweet tweet = Tweet.findById(tweets.get(j).id);
        tweeter.tweets.remove(tweet);
        tweeter.save();
        tweet.delete();   
      }
      tweeter.delete();
    }
    renderText("success");
  }
}