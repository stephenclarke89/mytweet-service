package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;
import play.db.jpa.GenericModel;

@Entity
public class Tweet extends GenericModel
{
  @Id
  public String   id;
  public String   messageTweet;
  public String   date;
  public String   contact;

  @ManyToOne
  public Tweeter tweeter;

  public Tweet(String id, String date, String message, String contact, Tweeter tweeter)
  {
    this.id = id;
    this.messageTweet = message;
    this.date = date;
    this.contact = contact;
    this.tweeter = tweeter;
  }
} 