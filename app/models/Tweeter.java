package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import play.db.jpa.GenericModel;

@Entity
public class Tweeter extends GenericModel
{
  @Id
  public String id;
  public String firstName;
  public String lastName;
  public String email;
  public String password;

  @OneToMany(mappedBy="tweeter", cascade = CascadeType.ALL)
  public List<Tweet> tweets = new ArrayList<Tweet>();

  public Tweeter(String id, String firstName, String lastName, String email, String password)
  {
    this.id     =	id;
    this.firstName= firstName;
    this.lastName = lastName;
    this.email    = email;
    this.password = password;
  }
  
  public static Tweeter findByEmail(String email)
  {
    return find("email", email).first();
  }

  public String getFullName()
  {
    return firstName + " " + lastName;
  }
  
  public boolean checkPassword(String password)
  {
    return this.password.equals(password);
  }
}